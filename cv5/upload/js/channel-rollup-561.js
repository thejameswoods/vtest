/*!=======================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/
// ***************************
// js/subscribe.js
// ***************************

// Cache phrases used in this file
window.vBulletin = window.vBulletin || {};
window.vBulletin.phrase = window.vBulletin.phrase || {};
window.vBulletin.phrase.precache = window.vBulletin.phrase.precache || [];
window.vBulletin.phrase.precache = $.merge(window.vBulletin.phrase.precache, [
	'follow',
	'following',
	'following_pending',
	'social_group_count_members_x',
	'unsubscribe_overlay_error'
]);

// Cache settings used in this file
window.vBulletin.options = window.vBulletin.options || {};
window.vBulletin.options.precache = window.vBulletin.options.precache || [];
window.vBulletin.options.precache = $.merge(window.vBulletin.options.precache, []);


(function()
{
	// Moved from global.js

	vBulletin.subscribe = {};

	/**
	 * Implements the subscribe functionality to the subscribe button
	 *
	 */
	var subscribe = function(button)
	{
		var $button = $(button);

		if ($button.hasClass('isSubscribed') || $button.hasClass('is-pending'))
		{
			return true;
		}

		var itemId = parseInt($button.attr('data-node-id'), 10),
			params = {},
			request = '';

		if ($button.hasClass('is-topic'))
		{
			request = '/ajax/api/follow/add';
			params['follow_item'] = itemId;
			params['type'] = 'follow_contents';
			subscribeRequest($button, request, params);
		}
		else
		{
			var data = {};
			data['channelid'] = itemId;
			data['recipient'] = parseInt($button.attr('data-owner-id'), 10);
			if ($button.hasClass('is-blog-channel'))
			{
				data['requestType'] = 'member'; // "subscribe" means join (and then also subscribe) for blogs.
			}
			else if ($button.hasClass('is-sg-channel'))
			{
				data['requestType'] = 'sg_subscriber';
			}
			else
			{
				data['requestType'] = 'subscriber';
			}

			subscribeRequest($button, '/ajax/api/node/requestChannel', data);
		}
	};

	var updateButtonText = function($button, buttonphrase)
	{
		$button.children('.js-button__text-primary').text(vBulletin.phrase.get(buttonphrase));
	};

	var updateButtonToSpecial = function($button, buttonphrase, buttonclass, removeclass)
	{
		updateButtonText($button, buttonphrase);
		$button.addClass((buttonclass || '') + ' b-button--special').removeClass((removeclass || '') + ' b-button--secondary');
	};

	var updateButtonToSecondary = function($button, buttonphrase, buttonclass, removeclass)
	{
		updateButtonText($button, buttonphrase);
		$button.addClass((buttonclass || '') + ' b-button--secondary').removeClass((removeclass || '') + ' b-button--special');
	};

	var subscribeRequest = function($button, request, params)
	{
		vBulletin.AJAX({
			call: request,
			data: params,
			success: function(response)
			{
				if (!isNaN(response) && response >= 1)
				{
					if (response == 1)
					{
						updateButtonToSpecial($button, 'following', 'isSubscribed');
					}
					else
					{
						updateButtonToSpecial($button, 'following_pending', 'is-pending');
					}
				}
				else
				{
					vBulletin.error('follow', 'follow_error');
				}
			},
			title_phrase: 'follow',
			error_phrase: 'follow_error'
		});
	};

	/**
	 * Updates the display value for the member count on the card.
	 *
	 * @param int Channel ID
	 * @param int The value to change the existing member count by (1 or -1)
	 */
	function updateDisplayedMemberCount(channelId, changeValue)
	{
		var $updateEl = $('.js-replace-member-count-' + channelId);
		if ($updateEl.length)
		{
			$updateEl.each(function()
			{
				var $this = $(this),
					phraseVarname = $this.data('replace-phrase-varname'),
					memberCount = parseInt($this.data('member-count'), 10),
					// I chose to increment or decrement the previous member
					// count instead of returning the actual current member
					// count from the API call, as I think this provides better
					// visual feedback to the user. They joined, so the count
					// should increase by one. If we pull the actual count
					// and update with that, the value might move by any amount
					// or not move at all (since other users may have joined or
					// left the group in the meantime) which is less useful
					// and less pleasing to the end user who just clicked.
					newMemberCount = memberCount + changeValue;

				// this assumes the phrase takes one parameter, and that
				// parameter is the member count.
				$this.text(vBulletin.phrase.get(phraseVarname, newMemberCount));
				$this.data('member-count', newMemberCount);
			});
		}

	}

	/**
	 * Implements the join button functionality
	 */
	var join = function(button)
	{
		var $button = $(button),
			itemId = parseInt($button.attr('data-node-id'), 10),
			recipientId = parseInt($button.attr('data-owner-id'), 10);

		vBulletin.AJAX({
			call: '/ajax/api/node/requestChannel',
			data: ({
				channelid : itemId,
				recipient : recipientId,
				requestType: 'sg_member'
			}),
			success: function(response)
			{
				if (response === true)
				{
					updateButtonToSpecial($button, 'joined', 'has-joined');
					updateDisplayedMemberCount(itemId, 1);

					if($button.hasClass('join-to-post-btn') || $button.hasClass('js-refresh'))
					{
						window.location.reload();
					}
				}
				else if (!isNaN(response))
				{
					updateButtonToSpecial($button, 'following_pending', 'is-pending');
				}
			},
			title_phrase: 'join',
			error_phrase: 'join_error'
		});
	};

	/**
	 * Implements the leave button functionality
	 *
	 */
	var leave = function(button)
	{
		var $button = $(button),
			itemId = parseInt($(button).attr('data-node-id'), 10);

		vBulletin.AJAX({
			call: '/ajax/api/blog/leaveChannel',
			data: {channelId : itemId},
			success: function(response)
			{
				if (response === true)
				{
					if ($button.hasClass('js-button--follow'))
					{
						updateButtonText($button, 'follow');
						$button.addClass('b-button--special').removeClass('isSubscribed b-button--unfollow');
					}
					else
					{
						updateButtonText($button, 'join');
						// rm the leave-btn class in case the user's mouse is still over the button (VBV-19297)
						$button.removeClass('has-joined b-button--special leave-btn');
					}

					updateDisplayedMemberCount(itemId, -1);

					if (!$button.is('.js-no-refresh'))
					{
						location.reload();
					}
				}
				else
				{
					vBulletin.error('leave', 'invalid_server_response_please_try_again');
				}
			},
			title_phrase: 'leave',
			error_phrase: 'invalid_server_response_please_try_again'
		});
	};

	/**
	 * Implements the unsubscribe functionality to the subscribe button
	 *
	 */
	var unsubscribe = function(button)
	{
		var $button = $(button),
			itemId = $(button).attr('data-node-id');

		vBulletin.AJAX({
			call: '/ajax/api/follow/delete',
			data: {
				follow_item : itemId,
				type : 'follow_contents'
			},
			success: function(response)
			{
				if (response == 1)
				{
					updateButtonToSecondary($button, 'follow', '', 'isSubscribed b-button--unfollow');
				}
				else
				{
					vBulletin.error('follow', 'unfollow_error');
				}
			},
			title_phrase: 'follow',
			error_phrase: 'unfollow_error'
		});
	};

	/**
	 * Updates the subscribe button to reflect the new subscribe status
	 *
	 * @param	int	The new subscribe status
	 * 				0 - Not subscribed
	 *				1 - Subscribed
	 *				2 - Pending subscription (usually this means it needs approval)
	 */
	vBulletin.subscribe.updateSubscribeButton = function(buttonStatus)
	{
		if (typeof buttonStatus != "undefined")
		{
			var $subscribeButton = $('.js-button--follow').filter(function()
			{
				return $(this).data('node-id') == pageData.nodeid && pageData.nodeid != pageData.channelid;
			});

			switch(buttonStatus)
			{
				case 0:
					updateButtonToSecondary($subscribeButton, 'follow', '', 'isSubscribed b-button--unfollow');
					break;

				case 1:
					updateButtonToSecondary($subscribeButton, 'following', 'isSubscribed');
					break;

				case 2:
					updateButtonToSecondary($subscribeButton, 'following_pending', 'is-pending');
					break;
			}
		}
	};

	// Initialization (moved from global.js)
	$('.js-button--follow').on('click', function(e)
	{
		if (!$(this).hasClass('isSubscribed'))
		{
			subscribe(this);
		}
		else if ($(this).hasClass('b-button--unfollow'))
		{
			if ($(this).hasClass('is-blog-channel'))
			{
				leave(this);
			}
			else
			{
				unsubscribe(this);
			}
		}
	});

	$('.js-button--follow').on('mouseover', function(e)
	{
		var $self = $(this);
		if ($self.hasClass('isSubscribed') && !$self.hasClass('is-owner'))
		{
			updateButtonToSecondary($self, 'following_remove', 'b-button--unfollow');
		}
	});

	$('.js-button--follow').on('mouseout', function(e)
	{
		var $self = $(this);
		if ($self.hasClass('b-button--unfollow'))
		{
			updateButtonToSpecial($self, 'following', '', 'b-button--unfollow');
		}
	});

	$('.join-to-post-btn').on('click', function(e)
	{
		if (!$(this).hasClass('has-joined') && !$(this).hasClass('is-pending'))
		{
			join(this);
		}
	});

	// the .join-btn also shows on the main group page, and the contents may
	// be loaded via AJAX due to pagination or switching the view (grid/list)

	$(document).on('click', '.join-btn', function(e)
	{
		if (!$(this).hasClass('has-joined') && !$(this).hasClass('is-pending'))
		{
			join(this);
		}
		else if ($(this).hasClass('leave-btn') && !$(this).hasClass('is-owner'))
		{
			leave(this);
		}
	});

	$(document).on('mouseover', '.join-btn', function(e)
	{
		var $self = $(this);
		if ($self.hasClass('has-joined') && !$self.hasClass('is-owner'))
		{
			updateButtonToSecondary($self, 'leave', 'leave-btn');
		}
	});

	$(document).on('mouseout', '.join-btn', function(e)
	{
		var $self = $(this);
		if ($self.hasClass('leave-btn') && !$self.hasClass('is-owner'))
		{
			updateButtonToSpecial($self, 'joined', '', 'leave-btn');
		}
	});

})();

;

/*=========================================================================*\
|| #######################################################################
|| # NulleD By - vBSupport.org
|| # CVS: $RCSfile$ - $Revision: 101267 $
|| #######################################################################
\*=========================================================================*/
