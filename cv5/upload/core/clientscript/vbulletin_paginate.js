/*=======================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/
(function(E){var D=".js-pagenum";function A(H,I){I=I||E(".js-toolbar-pagenav");I.find(D).val(H);I.find(".left-arrow, .right-arrow").removeClass("h-disabled");if(H<=1){I.find(".left-arrow").addClass("h-disabled")}var J=I.find(".js-maxpage").text();if(H>=J){I.find(".right-arrow").addClass("h-disabled")}if(1<=H&&H<=J){I.data("curpage",H)}}function B(N){var H=E(N.currentTarget),K=H.closest(".js-toolbar-pagenav"),I=K.closest('form[name="pagenavform"]'),J=parseInt(K.find(".js-maxpage").text(),10),M=parseInt(K.data("curpage"),10),L;if(H.hasClass("left-arrow")){L=M-1}else{if(H.hasClass("right-arrow")){L=M+1}else{if(H.is("input:text")||H.hasClass("textbox")){L=parseInt(H.val(),10)}else{return }}}if(isNaN(L)||(L>J)||(L<1)){A(M,K);return }A(L,K);I.submit()}function G(H){if(H.data("vb-paginate-init")){return }H.data("vb-paginate-init",1);A(parseInt(H.find(D).val(),10),H);H.find(".arrow").click(B);H.find(D).change(B);H.find(D).on("keypress",function(I){if(I.keyCode==13){I.preventDefault();I.target.blur()}});H.find(".js-perpage").change(function(J){var I=E(J.currentTarget).closest('form[name="pagenavform"]');A(1);I.submit()})}function C(){G(E(".js-toolbar-pagenav"))}function F(){E(C)}F()})(jQuery);