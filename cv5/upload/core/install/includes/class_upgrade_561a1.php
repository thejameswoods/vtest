<?php
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/
/*
if (!isset($GLOBALS['vbulletin']->db))
{
	exit;
}
*/

class vB_Upgrade_561a1 extends vB_Upgrade_Version
{
	/*Constants=====================================================================*/

	/*Properties====================================================================*/

	/**
	* The short version of the script
	*
	* @var	string
	*/
	public $SHORT_VERSION = '561a1';

	/**
	* The long version of the script
	*
	* @var	string
	*/
	public $LONG_VERSION = '5.6.1 Alpha 1';

	/**
	* Versions that can upgrade to this script
	*
	* @var	string
	*/
	public $PREV_VERSION = '5.6.0';

	/**
	* Beginning version compatibility
	*
	* @var	string
	*/
	public $VERSION_COMPAT_STARTS = '';

	/**
	* Ending version compatibility
	*
	* @var	string
	*/
	public $VERSION_COMPAT_ENDS = '';

	public function step_1()
	{
		$this->run_query(
			sprintf($this->phrase['vbphrase']['create_table'], TABLE_PREFIX . 'location'),
			"
				CREATE TABLE " . TABLE_PREFIX . "location (
					locationid INT UNSIGNED NOT NULL AUTO_INCREMENT,
					title VARCHAR(250) NOT NULL DEFAULT '',
					locationcodes TEXT,
					PRIMARY KEY (locationid)
				) ENGINE = " . $this->hightrafficengine . "
			",
			self::MYSQL_ERROR_TABLE_EXISTS
		);
	}

	public function step_2()
	{
		vB_Upgrade::createAdminSession();

		$library = vB_Library::instance('options');

		//assume that if we have any locations we are dealing with the
		//first upgrade or, at least, we need to restore the defaults.
		$locations = $library->getLocationList();
		if(!$locations)
		{
			$this->show_message(sprintf($this->phrase['vbphrase']['update_table'], TABLE_PREFIX . 'setting'));
			$california = array(
				'title' => 'California',
				'locationcodes' => array('US:CA', 'UNKNOWN'),
			);
			$library->saveLocation($california);

			$eu = array(
				'title' => 'European Union',
				'locationcodes' => array(
					'AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU',
					'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES',
					'SE', 'GB', 'AL', 'ME', 'RS', 'MK', 'TR', 'IS', 'LI', 'MC', 'NO', 'CH', 'UA', 'EU',
					'UNKNOWN'
				),
			);
			$euid = $library->saveLocation($eu);

			$options = vB::getDatastore()->getValue('options');

			$privacyoptions = array(
				'enable_privacy_guest',
				'enable_privacy_registered',
				'block_eu_visitors',
				'enable_account_removal',
			);

			//if the privacy option is enabled, then set it to the 'EU' location.
			$db = vB::getDbAssertor();
			$location = json_encode(array($euid));
			foreach($privacyoptions AS $privacyoption)
			{
				if($options[$privacyoption] == 1)
				{
					//don't use set_option here -- that depends on the option having the correct meta data
					//which it likely doesn't until we run final upgrade.  Likewise we need to make sure that the
					//datatype is "free" or the option import will stomp all over everything.
					$db->update('setting', array('value' => $location, 'datatype' => 'free'), array('varname' => $privacyoption));
				}
			}
		}
		else
		{
			$this->skip_message();
		}

		$this->long_next_step();
	}

	public function step_3()
	{
		$this->add_field(
			sprintf($this->phrase['core']['altering_x_table'], TABLE_PREFIX . 'user', 1, 2),
			'user',
			'location',
			'VARCHAR',
			array(
				'length' => 30,
				'default' => 'UNKNOWN',
			)
		);
	}

	public function step_4($data)
	{
		if ($this->field_exists('user', 'eustatus'))
		{
			if(empty($data['startat']))
			{
				$this->show_message(sprintf($this->phrase['vbphrase']['update_table_x'], TABLE_PREFIX . 'user', 1, 1));
			}

			$callback = function($startat, $nextid)
			{
				$db = vB::getDbAssertor();
				$db->update(
					'user',
					array('location' => 'EU'),
					array(
						'eustatus' => 1,
						array('field' => 'userid', 'value' => $startat, 'operator' =>  vB_dB_Query::OPERATOR_GTE),
						array('field' => 'userid', 'value' => $nextid, 'operator' =>  vB_dB_Query::OPERATOR_LT),
					)
				);
			};

			$newdata = $this->updateByIdWalk($data,	20000, 'vBInstall:getMaxUserid', 'user', 'userid', $callback);

			//this is the last iteration.
			if(!$newdata)
			{
				$this->long_next_step();
			}

			return $newdata;
		}
		else
		{
			$this->skip_message();
			$this->long_next_step();
		}
	}


	public function step_5()
	{
		$this->drop_field(
			sprintf($this->phrase['core']['altering_x_table'], TABLE_PREFIX . 'user', 2, 2),
			'user',
			'eustatus'
		);
	}

	public function step_6()
	{
		$db = vB::getDbAssertor();
		$this->show_message(sprintf($this->phrase['vbphrase']['update_table_x'], TABLE_PREFIX . 'ipaddressinfo', 1, 1));
		$db->assertQuery('truncateTable', array('table' => 'ipaddressinfo'));
	}

	public function step_7()
	{
		$this->add_field(
			sprintf($this->phrase['core']['altering_x_table'], 'ipaddressinfo', 1, 2),
			'ipaddressinfo',
			'location',
			'VARCHAR',
			array(
				'length' => 30,
				'default' => '',
			)
		);
	}

	public function step_8()
	{
		$this->drop_field(
			sprintf($this->phrase['core']['altering_x_table'], TABLE_PREFIX . 'ipaddressinfo', 2, 2),
			'ipaddressinfo',
			'eustatus'
		);
	}
}

/*======================================================================*\
|| ####################################################################
|| # NulleD By - vBSupport.org
|| # CVS: $RCSfile$ - $Revision: 104786 $
|| ####################################################################
\*======================================================================*/
