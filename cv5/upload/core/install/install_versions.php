<?php
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

/**
 *	@package vBInstall
 */

/*
 *	Holds the various minimum versions for vbulletin.  Versions below these will
 *	cause the installer to fail.
 *
 *	If you change these to install vBulletin on an unsupported version, vBulletin
 *	may not work properly or may not work at all.
 */
$install_versions = array(
	'php_required' => '7.2.0',
	'mysql_required' => '5.5.8',
	'mariadb_required' => '5.5.8',
);

/*=========================================================================*\
|| #######################################################################
|| # NulleD By - vBSupport.org
|| # CVS: $RCSfile$ - $Revision: 103807 $
|| #######################################################################
\*=========================================================================*/
