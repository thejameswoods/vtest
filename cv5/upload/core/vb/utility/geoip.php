<?php
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

/**
 * vB_Utility_Geoip
 *
 * @package vBulletin
 */
abstract class vB_Utility_Geoip
{
	use vB_Utility_Trait_NoSerialize;

	//this is terrible, but better than nothing for old implemenations
	public function getLocation($ipaddress)
	{
		return $this->getCountryCode($ipaddress);
	}

	/**
	 *	Return the country code from the GeoIP provider.
	 *
	 *	@param string $address -- The IP address to check
	 *	@return string -- the country code returned from the IP Provider
	 */
	//this is obsolete but leaving in place so that previous geoip implementations
	//might work after a fashion.  It can be removed after a while.
	//we should no longer *expect* children to implement it -- they should override
	//getLocation instead.
	protected function getCountryCode($ipaddress)
	{
		return 'UNKNOWN';
	}

	/**
	 *	Return the raw response from the GeoIP provider.
	 *
	 *	This is public to allow for diagnostic testing.  It is not intended for general use.
	 *
	 *	@param string $address -- The IP address to check
	 *	@return string -- the raw text of the response (presumable JSON or some other serialization format)
	 */
	public function getIpData($address)
	{
		return "Raw data not available";
	}

}

/*=========================================================================*\
|| #######################################################################
|| # NulleD By - vBSupport.org
|| # CVS: $RCSfile$ - $Revision: 104224 $
|| #######################################################################
\*=========================================================================*/
