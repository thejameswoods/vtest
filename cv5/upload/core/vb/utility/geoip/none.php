<?php
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

/**
 * vB_Utility_Geoip
 *
 * @package vBulletin
 */
class vB_Utility_Geoip_None extends vB_Utility_Geoip
{
	public function getLocation($address)
	{
		return 'UNKNOWN';
	}

	/**
	 *	This is public to allow for diagnostic testing.  It is not intended for general use.
	 */
	public function getIpData($address)
	{
		return 'No Geoip Provider has been configured';
	}
}

/*=========================================================================*\
|| #######################################################################
|| # NulleD By - vBSupport.org
|| # CVS: $RCSfile$ - $Revision: 103964 $
|| #######################################################################
\*=========================================================================*/
