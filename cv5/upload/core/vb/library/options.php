<?php if (!defined('VB_ENTRY')) die('Access denied.');
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

/**
 * vB_Library_Options
 *
 * @package vBLibrary
 * @access public
 */
class vB_Library_Options extends vB_Library
{
	public function updateValue($varname, $value, $rebuild = true)
	{
		require_once(DIR . '/includes/functions_file.php');
		require_once(DIR . '/includes/adminfunctions_options.php');
		$retVal = vB::getDbAssertor()->update('setting', array('value' => $value), array('varname' => $varname));

		if ($rebuild)
		{
			vB::getDatastore()->build_options();
		}

		return array('success' => $retVal);
	}

	/**
	 * This function updates specified settings
	 * @param array $values
	 *	'varname' => $vbulletin->GPC['varname'],
	 *	'grouptitle' => $vbulletin->GPC['grouptitle'],
	 *	'optioncode' => $vbulletin->GPC['optioncode'],
	 *	'defaultvalue' => $vbulletin->GPC['defaultvalue'],
	 *	'displayorder' => $vbulletin->GPC['displayorder'],
	 *	'volatile' => $vbulletin->GPC['volatile'],
	 *	'datatype' => $vbulletin->GPC['datatype'],
	 *	'validationcode' => $vbulletin->GPC['validationcode'],
	 *	'product' => $vbulletin->GPC['product'],
	 *	'blacklist' => $vbulletin->GPC['blacklist'],
	 *	'title' => $vbulletin->GPC['title'],
	 *	'username' => $vbulletin->userinfo['username'],
	 *	'description' => $vbulletin->GPC['description']
	 * @return array, $response
	 */
	public function updateSetting($values)
	{
		require_once(DIR . '/includes/functions_file.php');
		require_once(DIR . '/includes/adminfunctions_options.php');
		require_once(DIR . '/includes/adminfunctions.php');
		$response = array();
		$langid = $values['volatile'] ? -1 : 0;

		vB::getDbAssertor()->assertQuery('setting',
		array(vB_dB_Query::TYPE_KEY => vB_dB_Query::QUERY_UPDATE,
			'grouptitle' => $values['grouptitle'],
			'optioncode' => $values['optioncode'],
			'defaultvalue' => $values['defaultvalue'],
			'displayorder' => $values['displayorder'],
			'volatile' => $values['volatile'],
			'datatype' => $values['datatype'],
			'validationcode' => $values['validationcode'],
			'product' => $values['product'],
			'blacklist' => $values['blacklist'],
			'ispublic' => $values['ispublic'],
			'adminperm' => (isset($values['adminperm']) ? $values['adminperm'] : ''),
			vB_dB_Query::CONDITIONS_KEY => array(
				array('field' => 'varname', 'value' => $values['varname'], 'operator' => vB_dB_Query::OPERATOR_EQ)
			)
		)
		);

		$phrases = vB::getDbAssertor()->assertQuery('vBForum:phrase', array(
			vB_dB_Query::TYPE_KEY => vB_dB_Query::QUERY_SELECT,
			'languageid' => array(-1,0),
			'fieldname' => 'vbsettings',
			'varname' => array("setting_" . $values['varname'] . "_title", "setting_" . $values['varname'] . "_desc")
		));

		$full_product_info = fetch_product_list(true);
		$product_version = $full_product_info[$values['product']]['version'];

		if ($phrases AND $phrases->valid())
		{
			$timeNow = vB::getRequest()->getTimeNow();

			foreach ($phrases AS $phrase)
			{
				if ($phrase['varname'] == "setting_" . $values['varname'] . "_title")
				{
					vB::getDbAssertor()->assertQuery('vBForum:phrase',
					array(vB_dB_Query::TYPE_KEY => vB_dB_Query::QUERY_UPDATE,
						'languageid' => $langid,
						'text' => $values['title'],
						'product' => $values['product'],
						'username' => $values['username'],
						'dateline' => $timeNow,
						'version' => $product_version,
						vB_dB_Query::CONDITIONS_KEY => array(
							array('field' => 'languageid', 'value' => $phrase['languageid'], 'operator' => vB_dB_Query::OPERATOR_EQ),
							array('field' => 'varname', 'value' => "setting_" . $values['varname'] . "_title" , 'operator' => vB_dB_Query::OPERATOR_EQ)
						)
					)
					);
				}
				else if ($phrase['varname'] == "setting_" . $values['varname'] . "_desc")
				{
					vB::getDbAssertor()->assertQuery('vBForum:phrase',
					array(vB_dB_Query::TYPE_KEY => vB_dB_Query::QUERY_UPDATE,
						'languageid' => $langid,
						'text' => $values['description'],
						'product' => $values['product'],
						'username' => $values['username'],
						'dateline' => $timeNow,
						'version' => $product_version,
						vB_dB_Query::CONDITIONS_KEY => array(
							array('field' => 'languageid', 'value' => $phrase['languageid'], 'operator' => vB_dB_Query::OPERATOR_EQ),
							array('field' => 'varname', 'value' => "setting_" . $values['varname'] . "_desc" , 'operator' => vB_dB_Query::OPERATOR_EQ)
						)
					)
					);
				}
			}
		}

		vB::getDatastore()->build_options();
		$response['update'] = true;
		return $response;
	}

	//this is a hack.  We need to sort out handling "array" options in a systematic
	//way but it appears that we never have before.  This, at least, sweeps the nasty
	//little details under the rug.
	public function getLocationOption($optionname)
	{
		$datastore = vB::getDatastore();
		$value = $datastore->getOption($optionname);
		$value = json_decode($value, true);

		if(!is_array($value))
		{
			$value = array();
		}

		return $value;
	}

	public function isLocationOptionSet($optionname)
	{
		$option = $this->getLocationOption($optionname);
		return count($option);
	}

	/*
	 *	Locations aren't worth their own library/api classes and are strongly tied to the privacy settings.
	 *	So let's put this here.
	 */
	public function getLocationList()
	{
		$db = vB::getDbAssertor();
		$locations = $db->getRows('location', array(vB_dB_Query::COLUMNS_KEY => array('locationid', 'title')), 'title', 'locationid');
		return $locations;
	}

	public function getLocationCache()
	{
		$datastore = vB::getDatastore();
		$locations = $datastore->getValue('locations');

		if(!is_array($locations))
		{
			$locations = $this->updateLocationCache();
		}

		return $locations;
	}

	//we could probably fix this to allow updating deltas without the extra DB queries since
	//we'll have the info in most cases to adjust the cache without the extra query (aside
	//from the initial load or a hard rebuild) but it shouldn't change very much or in
	//performance critical contexts.  Doing this way makes it less likely that something will
	//get out of sync and not recover.
	//
	//we only really need to store any locations used in the options, but that would require
	//updating the cache any time the options change.  This is simpler and I don't anticipate the
	//locations getting out of hand.
	private function updateLocationCache()
	{
		$db = vB::getDbAssertor();
		$locations = $db->getColumn('location', 'locationcodes', array(), false, 'locationid');

		foreach($locations AS $key => $locationCodes)
		{
			$locations[$key] = explode(',', $locationCodes);
		}

		vB::getDatastore()->build('locations', serialize($locations), 1);
		return $locations;
	}

	public function getLocation($locationid)
	{
		$db = vB::getDbAssertor();
		$location = $db->getRow('location', array('locationid' => $locationid));

		if(!$location)
		{
			throw new vB_Exception_Api('invalid_data_w_x_y_z', array($locationid, '$locationid', __CLASS__, __FUNCTION__));
		}

		$location['locationcodes'] = explode(',', $location['locationcodes']);
		return $location;
	}

	public function saveLocation($data)
	{
		if(is_array($data['locationcodes']))
		{
			$data['locationcodes'] = implode(',', array_map('trim', $data['locationcodes']));
		}

		if(empty($data['title']))
		{
			throw new vB_Exception_Api('invalid_title_specified');
		}

		$db = vB::getDbAssertor();
		if(isset($data['locationid']))
		{
			$values = $data;
			$values[vB_dB_Query::TYPE_KEY] = vB_dB_Query::QUERY_UPDATE;
			$db->assertQuery('location', $values);
			$id = $data['locationid'];
		}
		else
		{
			$id = $db->insert('location', $data);
		}

		$this->updateLocationCache();
		return $id;
	}

	public function deleteLocation($locationid)
	{
		$locationSettings = array(
			'enable_privacy_guest',
			'enable_privacy_registered',
			'block_eu_visitors',
			'enable_account_removal',
		);

		foreach($locationSettings AS $setting)
		{
			$locOption = $this->getLocationOption($setting);
			if(in_array($locationid, $locOption))
			{
				throw new vB_Exception_Api('location_is_in_use');
			}
		}

		$db = vB::getDbAssertor();
		$db->delete('location', array('locationid' => $locationid));

		$this->updateLocationCache();
	}

	/**
	 * Return an array of location codes for countries.
	 *
	 * Currently we hardcode the name are returned as English names.  In the future this may change.
	 *
	 * @return array -- of the form code => country name
	 */
	public function getCountryCodes()
	{
		$countries = array(
			'AD' => 'Andorra',
			'AE' => 'United Arab Emirates',
			'AF' => 'Afghanistan',
			'AG' => 'Antigua and Barbuda',
			'AI' => 'Anguilla',
			'AL' => 'Albania',
			'AM' => 'Armenia',
			'AO' => 'Angola',
			'AQ' => 'Antarctica',
			'AR' => 'Argentina',
			'AS' => 'American Samoa',
			'AT' => 'Austria',
			'AU' => 'Australia',
			'AW' => 'Aruba',
			'AX' => 'Aland Islands',
			'AZ' => 'Azerbaijan',
			'BA' => 'Bosnia and Herzegovina',
			'BB' => 'Barbados',
			'BD' => 'Bangladesh',
			'BE' => 'Belgium',
			'BF' => 'Burkina Faso',
			'BG' => 'Bulgaria',
			'BH' => 'Bahrain',
			'BI' => 'Burundi',
			'BJ' => 'Benin',
			'BL' => 'Saint Barthelemy',
			'BM' => 'Bermuda',
			'BN' => 'Brunei',
			'BO' => 'Bolivia',
			'BQ' => 'Bonaire, Sint Eustatius, and Saba',
			'BR' => 'Brazil',
			'BS' => 'Bahamas',
			'BT' => 'Bhutan',
			'BV' => 'Bouvet Island',
			'BW' => 'Botswana',
			'BY' => 'Belarus',
			'BZ' => 'Belize',
			'CA' => 'Canada',
			'CC' => 'Cocos (Keeling) Islands',
			'CD' => 'Democratic Republic of the Congo',
			'CF' => 'Central African Republic',
			'CG' => 'Congo',
			'CH' => 'Switzerland',
			'CI' => 'Ivory Coast',
			'CK' => 'Cook Islands',
			'CL' => 'Chile',
			'CM' => 'Cameroon',
			'CN' => 'China',
			'CO' => 'Colombia',
			'CR' => 'Costa Rica',
			'CU' => 'Cuba',
			'CV' => 'Cabo Verde',
			'CW' => 'Curacao',
			'CX' => 'Christmas Island',
			'CY' => 'Cyprus',
			'CZ' => 'Czechia',
			'DE' => 'Germany',
			'DJ' => 'Djibouti',
			'DK' => 'Denmark',
			'DM' => 'Dominica',
			'DO' => 'Dominican Republic',
			'DZ' => 'Algeria',
			'EC' => 'Ecuador',
			'EE' => 'Estonia',
			'EG' => 'Egypt',
			'EH' => 'Western Sahara',
			'ER' => 'Eritrea',
			'ES' => 'Spain',
			'ET' => 'Ethiopia',
			'FI' => 'Finland',
			'FJ' => 'Fiji',
			'FK' => 'Falkland Islands (Malvinas)',
			'FM' => 'Micronesia',
			'FO' => 'Faroe Islands',
			'FR' => 'France',
			'GA' => 'Gabon',
			'GB' => 'United Kingdom',
			'GD' => 'Grenada',
			'GE' => 'Georgia',
			'GF' => 'French Guiana',
			'GG' => 'Guernsey',
			'GH' => 'Ghana',
			'GI' => 'Gibraltar',
			'GL' => 'Greenland',
			'GM' => 'Gambia',
			'GN' => 'Guinea',
			'GP' => 'Guadeloupe',
			'GQ' => 'Equatorial Guinea',
			'GR' => 'Greece',
			'GS' => 'South Georgia and the South Sandwich Islands',
			'GT' => 'Guatemala',
			'GU' => 'Guam',
			'GW' => 'Guinea-Bissau',
			'GY' => 'Guyana',
			'HK' => 'Hong Kong',
			'HM' => 'Heard Island and McDonald Islands',
			'HN' => 'Honduras',
			'HR' => 'Croatia',
			'HT' => 'Haiti',
			'HU' => 'Hungary',
			'ID' => 'Indonesia',
			'IE' => 'Ireland',
			'IL' => 'Israel',
			'IM' => 'Isle of Man',
			'IN' => 'India',
			'IO' => 'British Indian Ocean Territory',
			'IQ' => 'Iraq',
			'IR' => 'Iran',
			'IS' => 'Iceland',
			'IT' => 'Italy',
			'JE' => 'Jersey',
			'JM' => 'Jamaica',
			'JO' => 'Jordan',
			'JP' => 'Japan',
			'KE' => 'Kenya',
			'KG' => 'Kyrgyzstan',
			'KH' => 'Cambodia',
			'KI' => 'Kiribati',
			'KM' => 'Comoros',
			'KN' => 'Saint Kitts and Nevis',
			'KP' => 'North Korea',
			'KR' => 'South Korea',
			'KW' => 'Kuwait',
			'KY' => 'Cayman Islands',
			'KZ' => 'Kazakhstan',
			'LA' => 'Laos',
			'LB' => 'Lebanon',
			'LC' => 'Saint Lucia',
			'LI' => 'Liechtenstein',
			'LK' => 'Sri Lanka',
			'LR' => 'Liberia',
			'LS' => 'Lesotho',
			'LT' => 'Lithuania',
			'LU' => 'Luxembourg',
			'LV' => 'Latvia',
			'LY' => 'Libya',
			'MA' => 'Morocco',
			'MC' => 'Monaco',
			'MD' => 'Moldova, Republic of',
			'ME' => 'Montenegro',
			'MF' => 'Saint Martin (French part)',
			'MG' => 'Madagascar',
			'MH' => 'Marshall Islands',
			'MK' => 'North Macedonia',
			'ML' => 'Mali',
			'MM' => 'Myanmar',
			'MN' => 'Mongolia',
			'MO' => 'Macao',
			'MP' => 'Northern Mariana Islands',
			'MQ' => 'Martinique',
			'MR' => 'Mauritania',
			'MS' => 'Montserrat',
			'MT' => 'Malta',
			'MU' => 'Mauritius',
			'MV' => 'Maldives',
			'MW' => 'Malawi',
			'MX' => 'Mexico',
			'MY' => 'Malaysia',
			'MZ' => 'Mozambique',
			'NA' => 'Namibia',
			'NC' => 'New Caledonia',
			'NE' => 'Niger',
			'NF' => 'Norfolk Island',
			'NG' => 'Nigeria',
			'NI' => 'Nicaragua',
			'NL' => 'Netherlands',
			'NO' => 'Norway',
			'NP' => 'Nepal',
			'NR' => 'Nauru',
			'NU' => 'Niue',
			'NZ' => 'New Zealand',
			'OM' => 'Oman',
			'PA' => 'Panama',
			'PE' => 'Peru',
			'PF' => 'French Polynesia',
			'PG' => 'Papua New Guinea',
			'PH' => 'Philippines',
			'PK' => 'Pakistan',
			'PL' => 'Poland',
			'PM' => 'Saint Pierre and Miquelon',
			'PN' => 'Pitcairn',
			'PR' => 'Puerto Rico',
			'PS' => 'Palestine, State of',
			'PT' => 'Portugal',
			'PW' => 'Palau',
			'PY' => 'Paraguay',
			'QA' => 'Qatar',
			'RE' => 'Reunion',
			'RO' => 'Romania',
			'RS' => 'Serbia',
			'RU' => 'Russian Federation',
			'RW' => 'Rwanda',
			'SA' => 'Saudi Arabia',
			'SB' => 'Solomon Islands',
			'SC' => 'Seychelles',
			'SD' => 'Sudan',
			'SE' => 'Sweden',
			'SG' => 'Singapore',
			'SH' => 'Saint Helena, Ascension and Tristan da Cunha',
			'SI' => 'Slovenia',
			'SJ' => 'Svalbard and Jan Mayen',
			'SK' => 'Slovakia',
			'SL' => 'Sierra Leone',
			'SM' => 'San Marino',
			'SN' => 'Senegal',
			'SO' => 'Somalia',
			'SR' => 'Suriname',
			'SS' => 'South Sudan',
			'ST' => 'Sao Tome and Principe',
			'SV' => 'El Salvador',
			'SX' => 'Sint Maarten (Dutch part)',
			'SY' => 'Syrian Arab Republic',
			'SZ' => 'Eswatini',
			'TC' => 'Turks and Caicos Islands',
			'TD' => 'Chad',
			'TF' => 'French Southern Territories',
			'TG' => 'Togo',
			'TH' => 'Thailand',
			'TJ' => 'Tajikistan',
			'TK' => 'Tokelau',
			'TL' => 'Timor-Leste',
			'TM' => 'Turkmenistan',
			'TN' => 'Tunisia',
			'TO' => 'Tonga',
			'TR' => 'Turkey',
			'TT' => 'Trinidad and Tobago',
			'TV' => 'Tuvalu',
			'TW' => 'Taiwan',
			'TZ' => 'Tanzania',
			'UA' => 'Ukraine',
			'UG' => 'Uganda',
			'UM' => 'United States Minor Outlying Islands',
			'US' => 'United States',
			'UY' => 'Uruguay',
			'UZ' => 'Uzbekistan',
			'VA' => 'Vatican',
			'VC' => 'Saint Vincent and the Grenadines',
			'VE' => 'Venezuela',
			'VG' => 'Virgin Islands (British)',
			'VI' => 'Virgin Islands (U.S.)',
			'VN' => 'Vietnam',
			'VU' => 'Vanuatu',
			'WF' => 'Wallis and Futuna',
			'WS' => 'Samoa',
			'YE' => 'Yemen',
			'YT' => 'Mayotte',
			'ZA' => 'South Africa',
			'ZM' => 'Zambia',
			'ZW' => 'Zimbabwe',
			//Some IPs come from a "generic europe" source and some geoip providers will return an "EU" code for these.
			'EU' => 'Unknown Europe',
		);

		return $countries;
	}

	/**
	 *	Get the region codes for a country
	 *
	 *	Geoip providers are not consistent with the subregion codes returned for non US countries
	 *	(many countries have region structures that don't really fit in the country, state paradigm
	 *	that geoip providers tend to use and thus what the "regions" are end up ambiguous and different
	 *	implementations make different decisions).  Given that, and the fact that we don't have explicit
	 *	requirements for subregion designation outside of the US right now,
	 *
	 * @param string $countrycode -- The country to get the sub regions for.  If we don't have supported
	 * 	subregions for that country then an empty array will be returned.  Currently only the US is supported.
	 * @return array -- of the form countrycode:regioncode => name
	 */
	public function getRegionCodes($countrycode)
	{
		$regions = array(
			'US' => array(
				'US:AL' => 'Alabama',
				'US:AK' => 'Alaska',
				'US:AZ' => 'Arizona',
				'US:AR' => 'Arkansas',
				'US:CA' => 'California',
				'US:CO' => 'Colorado',
				'US:CT' => 'Connecticut',
				'US:DE' => 'Delaware',
				'US:DC' => 'District of Columbia',
				'US:FL' => 'Florida',
				'US:GA' => 'Georgia',
				'US:HI' => 'Hawaii',
				'US:ID' => 'Idaho',
				'US:IL' => 'Illinois',
				'US:IN' => 'Indiana',
				'US:IA' => 'Iowa',
				'US:KS' => 'Kansas',
				'US:KY' => 'Kentucky',
				'US:LA' => 'Louisiana',
				'US:ME' => 'Maine',
				'US:MD' => 'Maryland',
				'US:MA' => 'Massachusetts',
				'US:MI' => 'Michigan',
				'US:MN' => 'Minnesota',
				'US:MS' => 'Mississippi',
				'US:MO' => 'Missouri',
				'US:MT' => 'Montana',
				'US:NE' => 'Nebraska',
				'US:NV' => 'Nevada',
				'US:NH' => 'New Hampshire',
				'US:NJ' => 'New Jersey',
				'US:NM' => 'New Mexico',
				'US:NY' => 'New York',
				'US:NC' => 'North Carolina',
				'US:ND' => 'North Dakota',
				'US:OH' => 'Ohio',
				'US:OK' => 'Oklahoma',
				'US:OR' => 'Oregon',
				'US:PA' => 'Pennsylvania',
				'US:RI' => 'Rhode Island',
				'US:SC' => 'South Carolina',
				'US:SD' => 'South Dakota',
				'US:TN' => 'Tennessee',
				'US:TX' => 'Texas',
				'US:UT' => 'Utah',
				'US:VT' => 'Vermont',
				'US:VA' => 'Virginia',
				'US:WA' => 'Washington',
				'US:WV' => 'West Virginia',
				'US:WI' => 'Wisconsin',
				'US:WY' => 'Wyoming',
			),
		);

		return $regions[$countrycode] ?? array();
	}
}
/*=========================================================================*\
|| #######################################################################
|| # NulleD By - vBSupport.org
|| # CVS: $RCSfile$ - $Revision: 104164 $
|| #######################################################################
\*=========================================================================*/
