Note: You may need to make some adjustments depending on the flavor of your unix system.
Install Sphinx
--------------

- Login as root

- Sphinx is available through its official Web site at http://sphinxsearch.com/downloads (minimum version is 2.1.2, however support for versions prior to 2.2.4 will be dropped in a future version of vBulletin)

- unpack the package

- cd sphinx-<version>

- ./configure --enable-id64 --prefix=/usr/local/sphinx

- make

- Did you get Error 127 during step 5? Install gcc-c++ using the following command and then repeat step 5.
yum install gcc-c++
OR For Debian and Ubuntu flavors of *nix:
sudo apt-get install gcc


- make install

- Did you get dependency errors with steps 5 or 7? Install mysql-devel using the following command and then repeat steps 5 and 7.
yum install mysql-devel
OR For Debian and Ubuntu flavors of *nix:
sudo apt-get install libmysqlclient-dev

- Create the following directories in your sphinx install (/usr/local/sphinx/): log & data
	mkdir /usr/local/sphinx/log
	mkdir /usr/local/sphinx/data

Configure for vBulletin
-----------------------

- Upload the contents of the upload folder to the vbulletin root directory.
- At the end of your vbulletin core/includes/config.php file, add the following:
/*
 * Sphinx configuration parameters
 */
$config['Misc']['sphinx_host']		= '127.0.0.1';
$config['Misc']['sphinx_port']		= '9306';
$config['Misc']['sphinx_path']		= '/usr/local/sphinx'; //no trailing slash
$config['Misc']['sphinx_config']	= $config['Misc']['sphinx_path'] . '/etc/vbulletin-sphinx.php';

- Upload vbulletin-sphinx.php to /usr/local/sphinx/etc/ .

- Update the first line in vbulletin-sphinx.php to match your php installation path

- Change $myforumroot in vbulletin-sphinx.php to the exact forum root (Example: /home/vbulletin/public_html).

- Set vbulletin-sphinx.php file to executable using:
chmod +x /usr/local/sphinx/etc/vbulletin-sphinx.php

- Change directory to your sphinx folder using: cd /usr/local/sphinx

- Start the daemon using:
/usr/local/sphinx/bin/searchd --config /usr/local/sphinx/etc/vbulletin-sphinx.php

- Ignore the worning about 'vbulletin_disk' index, it will be created once you ran the indexer.

- To verify your Sphinx is working you can enter: ps ax | grep search[d]
If the above returned something such as: /usr/local/sphinx/bin/searchd --config... the daemon is running.

- Go to your AdminCP->Options->Search Type. In the drop down, select Sphinx Search, then hit go.

- That is it! Sphinx should now be working correctly on your board.


Note: Do not upload your vbulletin-sphinx.php to a web accessible URL. Doing so would give away your database details. If you follow these
exact instructions, it will not be in a web accessible URL.

Another Note: If you adjust any of your config.php credentials you will need to bounce your sphinx.
