#!/usr/local/bin/php -q
<?php
/**********
	BEGIN FORUM SPECIFIC CONFIGS
 *********/
/*
Set $myforumroot to the absolute path to your forum directory.
Example:
$myforumroot = '/home/username/public_html';
*/
$myforumroot = '/path/to/forum/root';


/*
Extended character support.

SphinxSearch only supports "latin and cyrillic" (English, Russian)
characters by default. If your forum is on utf8mb4 and expect
the searchable content to have characters outside of the default
(e.g. German, Korean, Arabic) you have to tell Sphinx to treat those
characters as characters instead of delimiters. This is done by setting
the "charset_table" option when generating the index.

For more information about charset tables, see sphinx docs:
http://sphinxsearch.com/docs/current.html#conf-charset-table

For a list of commonly used characters for a given language,
see sphinx wiki:
http://www.sphinxsearch.com/wiki/doku.php?id=charset_tables

Note that each time you change charset_table, you must rebuild the
disk index in entirety.

The charset_table values can be quite long, and often times it must
be broken up into multiple lines or else indexer or service/daemon
fails to parse the sphinx .conf file properly. This script tries
to facilate the process of setting & updating the supported
characters slightly easier by allowing you to provide charset files
rather than modifying the sphinx configuration directly.

For example, in order to support Chinese, Japanese & Korean characters
in sphinx search, go to
http://www.sphinxsearch.com/wiki/doku.php?id=charset_tables#cjk
and copy the entire block (as of this time, over 25,000 characters
starting with U+F900->U+8C48, and ending with U+A492..U+A4C6) into
a new file called "sphinxcharsets_CJK" (or any other filename with
the prefix of $charsetTablesPrefix). Set $charsetTablesFolder to
the folder (full, absolute path) containing this "sphinxcharsets_CJK"
file. Re-run this script and save its output to a sphinx .conf file,
then re-install the daemon/service specifying the new .conf file &
re-run indexer to generate the new disk index that understands the
CJK characters. Finally, truncate the RT index & re-attach the disk
index, and search should work*** for these characters.

***CJK characters in particular have an issue with word-boundaries.
Enabling ngram-extraction to search by single characters rather
than full words (which might actually contain nonwords or multiple
words) may provide better usability, see
http://sphinxsearch.com/docs/current.html#conf-ngram-len
for more information.



Summary:
Add {$charsetTablesPrefix}... files with contents from the sphinx
wiki (
http://www.sphinxsearch.com/wiki/doku.php?id=charset_tables )
Set $charsetTablesFolder to the folder containing these files.
Note: Subdirectories will not be checked.

Example:
$charsetTablesFolder = '/home/username/public_html/sphinxsearchfiles';
$charsetTablesPrefix = 'sphinxcharsets_';
*/
$charsetTablesFolder = '';
$charsetTablesPrefix = 'sphinxcharsets_';

/*
Extended character support.
If data is stored with utf8mb4 encoding, sphinx needs to pull the data
with the same encoding for the initial index build for best results.

Set $utf8mb4 = true; for utf8mb4 boards.
*/
$utf8mb4 = false;


/**********
	END FORUM SPECIFIC CONFIGS
 *********/




/**********
	BEGIN SCRIPT
 *********/
/*########## DO NOT TOUCH ANYTHING BELOW THIS LINE ##########*/
ob_start();
chdir($myforumroot);
require_once('./config.php');
require_once($config['core_path'] . '/includes/config.php');

if ($utf8mb4)
{
	$utf8_query_pre = "sql_query_pre   = SET CHARACTER_SET_RESULTS=utf8mb4 \n\t" .
					  "sql_query_pre   = SET NAMES utf8mb4 \n";
}
else
{
	$utf8_query_pre = '';
}

$version = getSphinxVersion($config);

//this is deprecated and needs to be removed from the config to avoid
//nasty warnings.  On the other hand the default changed and it was
//removed fairly quickly, so we can't just remove it from the config
//without requiring very recent versions of Sphinx.  So we try to
//detect the sphinx version and tweak the config accordingly.
//
//if we can't detect the version then include it to be on the safe side
//hopefully in a little while we can update our minimum sphinx version
//and remove this.
$charset_type = 'charset_type = utf-8';
//there is not an actual 2.2.0 version of sphinx, but the sphinx versions
//are a bit odd (something like 2.2.11-id64-release) and don't play well
//with the equality comparisons. 2.1.9 allows the charset_type config param,
//2.2.1 doesn't so comparing with 2.2.0 will give the right result
if($version AND version_compare($version, '2.2.0') == 1)
{
	$charset_type = '';
}

$charset_table = getCharsetTable($charsetTablesFolder, $charsetTablesPrefix);

//avoid notices if optional port value isn't in the db.
$dbport = (!empty($config['MasterServer']['port']) ? $config['MasterServer']['port'] : 3306);

print "
source {$config['Database']['dbname']}_disk
{
	type			= mysql
	sql_host		= {$config['MasterServer']['servername']}
	sql_user		= {$config['MasterServer']['username']}
	sql_pass		= {$config['MasterServer']['password']}
	sql_db			= {$config['Database']['dbname']}
	sql_port		= $dbport	# optional, default is 3306
	{$utf8_query_pre}
	sql_query_pre		= CREATE TEMPORARY TABLE IF NOT EXISTS {$config['Database']['tableprefix']}temp_index ( \
					nodeid INT(11) NOT NULL DEFAULT 0,\
					caption TEXT, \
					PRIMARY KEY (nodeid) \
				)
	sql_query_pre		= INSERT INTO {$config['Database']['tableprefix']}temp_index SELECT n.parentid AS nodeid, GROUP_CONCAT(att.caption) AS caption FROM ( \
					(select a.nodeid, a.caption FROM {$config['Database']['tableprefix']}attach a) \
					union all \
					(select p.nodeid, p.caption FROM {$config['Database']['tableprefix']}photo p) \
				) AS att \
				JOIN {$config['Database']['tableprefix']}node n WHERE att.nodeid = n.nodeid \
				GROUP BY n.parentid
	sql_query_pre		= CREATE TEMPORARY TABLE IF NOT EXISTS {$config['Database']['tableprefix']}temp_poll ( \
					nodeid INT(11) NOT NULL DEFAULT 0,\
					question VARCHAR(100), \
					content TEXT, \
					PRIMARY KEY (nodeid)\
				)
	sql_query_pre		= INSERT INTO {$config['Database']['tableprefix']}temp_poll \
							SELECT n.nodeid, n.title, GROUP_CONCAT(po.title) AS content \
							FROM {$config['Database']['tableprefix']}poll o \
							LEFT JOIN {$config['Database']['tableprefix']}polloption po ON po.nodeid = o.nodeid \
							INNER JOIN {$config['Database']['tableprefix']}node n ON n.nodeid = o.nodeid \
							GROUP BY po.nodeid

	sql_query_pre		= CREATE TEMPORARY TABLE IF NOT EXISTS {$config['Database']['tableprefix']}temp_depth ( \
					nodeid INT(11) NOT NULL DEFAULT 0,\
					depth TEXT, \
					PRIMARY KEY (nodeid)\
				)
	sql_query_pre		= INSERT INTO {$config['Database']['tableprefix']}temp_depth SELECT child, CONCAT('{', GROUP_CONCAT('\"n', parent, '\":', depth), '}') AS depth FROM {$config['Database']['tableprefix']}closure GROUP BY child

	sql_query		= \
	SELECT n.nodeid, n.title, \
				CONCAT( \
					ifnull(t.rawtext, ''), ' ', \
					ifnull(n.description, ''), ' ', \
					ifnull(a.caption, ''), ' ', \
					ifnull(a.filename, ' '), \
					ifnull(i.note, ' '), \
					ifnull(i.actionreason, ' '), \
					ifnull(i.customreason, ' '), \
					ifnull(l.url_title, ' '), \
					ifnull(p.caption, ' '), \
					ifnull(v.url_title, ' '), \
					ifnull(pl.question, ' '), \
					ifnull(pl.content, ' '), \
					ifnull(at.caption, ' '), ' ',\
					ifnull(event.location, ''), \
					'' \
				) AS content, \
				n.contenttypeid, n.lastcontentid, n.parentid, n.starter, n.userid, n.authorname, n.setfor, n.showpublished,\
				n.approved, n.showapproved, n.viewperms, n.featured, n.inlist, n.protected, n.votes, n.lastcontent, n.created, \
			 	n.publishdate, n.unpublishdate, crc32(n.prefixid) AS prefixid, n.textcount, \
				st.parentid AS starterparent, st.userid AS starteruser, st.lastcontent AS starterlastcontent, \
				st.publishdate AS starterpublishdate, st.created AS startercreated, st.textcount AS startertextcount, \
				st.displayorder AS starterdisplayorder, st.votes AS startervotes, \
				st.title AS startertitlesort, st.authorname AS starterauthorname, \
				d.depth AS depth, \
				event.eventstartdate AS eventstartdate, event.eventenddate AS eventenddate, trending.weight AS trendingweight, \
				n.title AS titlesort \
			FROM {$config['Database']['tableprefix']}node AS n \
			LEFT JOIN {$config['Database']['tableprefix']}node st ON st.nodeid = n.starter \
			LEFT JOIN {$config['Database']['tableprefix']}text t ON t.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}attach a ON a.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}gallery g ON g.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}infraction i ON i.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}link l ON l.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}photo p ON p.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}video v ON v.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}temp_poll AS pl ON pl.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}temp_index AS at ON at.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}temp_depth AS d ON d.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}event AS event ON event.nodeid = n.nodeid \
			LEFT JOIN {$config['Database']['tableprefix']}trending AS trending ON trending.nodeid = n.nodeid

	sql_attr_uint		= contenttypeid
	sql_attr_uint		= lastcontentid
	sql_attr_uint		= parentid
	sql_attr_uint		= starter
	sql_attr_uint		= starterparent
	sql_attr_uint		= starteruser
	sql_attr_timestamp	= starterlastcontent
	sql_attr_timestamp	= starterpublishdate
	sql_attr_timestamp	= startercreated
	sql_attr_uint		= startertextcount
	sql_attr_uint		= starterdisplayorder
	sql_attr_uint		= startervotes
	sql_attr_string		= startertitlesort
	sql_attr_string		= starterauthorname
	sql_attr_multi		= uint closure from query; SELECT child, parent FROM {$config['Database']['tableprefix']}closure
	sql_attr_json		= depth
	sql_attr_uint		= userid
	sql_attr_string		= authorname
	sql_attr_uint		= setfor
	sql_attr_multi		= uint sentto from query; SELECT s.nodeid, s.userid FROM {$config['Database']['tableprefix']}sentto s WHERE deleted=0
	sql_attr_uint		= showpublished
	sql_attr_uint		= approved
	sql_attr_uint		= showapproved
	sql_attr_uint		= viewperms
	sql_attr_uint		= featured
	sql_attr_uint		= inlist
	sql_attr_uint		= protected
	sql_attr_uint		= votes
	sql_attr_timestamp	= lastcontent
	sql_attr_timestamp	= created
	sql_attr_timestamp	= publishdate
	sql_attr_timestamp	= unpublishdate
	sql_attr_uint		= prefixid
	sql_attr_multi		= uint tagid from query; SELECT tg.nodeid, tg.tagid tg FROM {$config['Database']['tableprefix']}tagnode tg
	sql_attr_uint		= textcount
	sql_attr_timestamp	= eventstartdate
	sql_attr_timestamp	= eventenddate
	sql_attr_uint		= trendingweight
	sql_attr_string		= titlesort
}


index {$config['Database']['dbname']}_disk
{
	source			= {$config['Database']['dbname']}_disk
	path			= {$config['Misc']['sphinx_path']}/data/{$config['Database']['dbname']}_disk
	docinfo			= extern
	$charset_type
	$charset_table
}


index {$config['Database']['dbname']}
{
	type			= rt
	rt_mem_limit		= 256M

	path			= {$config['Misc']['sphinx_path']}/data/{$config['Database']['dbname']}
	$charset_type

	rt_field		= title
	rt_field		= content
	rt_attr_uint		= contenttypeid
	rt_attr_uint		= lastcontentid
	rt_attr_uint		= parentid
	rt_attr_uint		= starter
	rt_attr_uint		= starterparent
	rt_attr_uint		= starteruser
	rt_attr_timestamp	= starterlastcontent
	rt_attr_timestamp	= starterpublishdate
	rt_attr_timestamp	= startercreated
	rt_attr_uint		= startertextcount
	rt_attr_uint		= starterdisplayorder
	rt_attr_uint		= startervotes
	rt_attr_string		= startertitlesort
	rt_attr_string		= starterauthorname
	rt_attr_multi		= closure
	rt_attr_json		= depth
	rt_attr_uint		= userid
	rt_attr_string		= authorname
	rt_attr_uint		= setfor
	rt_attr_multi		= sentto
	rt_attr_uint		= showpublished
	rt_attr_uint		= approved
	rt_attr_uint		= showapproved
	rt_attr_uint		= viewperms
	rt_attr_uint		= featured
	rt_attr_uint		= inlist
	rt_attr_uint		= protected
	rt_attr_uint		= votes
	rt_attr_timestamp	= lastcontent
	rt_attr_timestamp	= created
	rt_attr_timestamp	= publishdate
	rt_attr_timestamp	= unpublishdate
	rt_attr_uint		= prefixid
	rt_attr_multi		= tagid
	rt_attr_uint		= textcount
	rt_attr_timestamp	= eventstartdate
	rt_attr_timestamp	= eventenddate
	rt_attr_uint		= trendingweight
	rt_attr_string		= titlesort
}

indexer
{
	mem_limit		= 256M
}


searchd
{
	listen			= 9312
	listen			= {$config['Misc']['sphinx_port']}:mysql41
	log				= {$config['Misc']['sphinx_path']}/log/searchd.log
	read_timeout	= 5
	max_children	= 30
	pid_file		= {$config['Misc']['sphinx_path']}/log/searchd.pid
	seamless_rotate	= 1
	preopen_indexes	= 1
	unlink_old		= 1
	workers			= threads # for RT to work
	binlog_path		= {$config['Misc']['sphinx_path']}/data
}";



function getSphinxVersion($config)
{
	$command = $config['Misc']['sphinx_path'] . '/bin/indexer';
	exec($command, $output);

	if(preg_match('#Sphinx ([\w\-.]*)#', $output[0], $matches))
	{
		return $matches[1];
	}

	return false;
}

function getCharsetTable($charsetTablesFolder, $charsetTablesPrefix)
{
	$charsetTablesFolder = realpath($charsetTablesFolder);
	if (empty($charsetTablesFolder))
	{
		return '';
	}


	$charsetTableString = '';
	$charsetTableFiles = array();
	$delim = ',';
	$perline = 10;
	$newline = '\\';
	$trimchars = $delim . $newline . ' ' . "\n\r\t";

	// Note, we will not be checking recursively, ONLY the immediate files!!!
	$dirIterator = new DirectoryIterator($charsetTablesFolder);
	foreach($dirIterator AS $file)
	{
		if (
			$file->isFile() AND
			$file->isReadable() AND
			strpos($file->getFilename(), $charsetTablesPrefix) === 0
		)
		{
			$filename = $file->getPathname();
			$charsetTableFiles[] = $filename;
			$charsetTableString .=  file_get_contents($filename) . "$delim ";
		}
	}
	$charsetTableString = new_linify($charsetTableString, $delim, $perline, $newline);

	if (!empty($charsetTableString))
	{
		return 'charset_table		= ' . $charsetTableString;
	}
	else
	{
		return '';
	}
}

// Split a line of $delim-separated values into multiple lines
function new_linify($csv, $delim = ',', $perline = 15, $newline = '\\')
{
	$input = explode($delim, $csv);
	$input = array_map('trim', $input);
	// We've strung a bunch of file contents together with the delim,
	// so if one file happened to have a trailing delimiter we may have
	// an empty value, which we should remove.
	$input = array_filter($input);
	$input = array_chunk($input, $perline);
	$output = array();
	foreach ($input AS $__row)
	{
		if (!empty($__row))
		{
			$output[] = implode($delim . ' ', $__row);
		}
	}
	reset($output);

	// array_key_last is php 7.3+
	$keys = array_keys($output);
	$lastkey = end($keys);
	$output[$lastkey] = rtrim($output[$lastkey], ', ');
	$output = implode($delim . ' ' . $newline . "\n", $output);

	return $output;
}
?>
