<?php
/*
	This script will handle running the vbulletin schedule tasks
	(see the Scheduled Tasks menu in the Admincp).  This avoids
	having users trigger ajax calls when hitting the site.  It
	also avoids the problem where the scheduled tasks fail to
	run because there is not enough traffic to trigger all of the
	scripts in a timely fashion.

	To use do the following
	1. Copy the script to your webserver.  Preferable outside of the webroot
		directory.
	2. Set the $core variable to the path to your vb core directory
	3. Add an entry to your system cron to run the script (recommend running every 		minute)
		/path/to/php /path/to/file/vbcron.php
*/

//set this to your vb core directory
$core = '';

/*================== DO NOT EDIT BELOW THIS LINE ========================*/
require_once($core . '/vb/vb.php');
vB::init();

$options = getopt('', array('count::', 'varname::'));
$count = (isset($options['count']) ? $options['count'] : 1);
$varname = (isset($options['varname']) ? $options['varname'] : null);

$request = new vB_Request_Cli();
vB::setRequest($request);

//sign is as guest, we don't need elevated permissions to run cron
$request->createSessionForUser(0);

//if we have a varname then run it (will run even if not scheduled),
//otherwise run the next scheduled task
//count is meaningless and ignored if a specific cron is requested
if($varname)
{
	//use the library because the API requires admin permissions and this script doesn't
	//log in as an admin user.  Since one of the use cases for this is to troubleshoot
	//cron issues when the calling user isn't an admin (which is the case when using the
	//admincp equivilant) doing so would be counterproductive.
	vB_Library::instance('cron')->runByVarname($varname);
}
else
{
	$cronapi = vB_Api::instance('cron');

	for ($i = 0; $i < $count; $i++)
	{
		$cronapi->run();
	}
}
