This is a sample package implementing a social sharing button on the topic page.

This is a duplicate of the Facebook share button that is already present, but demonstrates how to do it without editing VB core code.

Note that this uses the Facebook iframe method of adding the sharing button, which will cause performance issues when multiple buttons are shown on the same page.
