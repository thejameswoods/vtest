<?php if (!defined('VB_ENTRY')) die('Access denied.');
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

class examplefilescan_Utility_Filescanner_Blockword extends vB_Utility_Filescanner
{
	protected $blockedWords = array();

	protected function initialize($vboptions)
	{
		$words = $vboptions['examplefilescan_blockwords'] ?? '';
		$words = explode(',', $words);
		foreach ($words AS $word)
		{
			$word = trim($word);
			if (!empty($word))
			{
				$this->blockedWords[] = $word;
			}
		}
	}

	protected function checkDependencies($vboptions)
	{
		return true;
	}

	public function scanFile($filename)
	{
		// This is just for illustration. Don't do this as it can be slow.
		$contents = file_get_contents($filename);
		foreach ($this->blockedWords AS $word)
		{
			if (strpos($contents, $word) !== false)
			{
				return false;
			}
		}

		return true;
	}
}

/*=========================================================================*\
|| #######################################################################
|| # NulleD By - vBSupport.org
|| # CVS: $RCSfile$ - $Revision: 102615 $
|| #######################################################################
\*=========================================================================*/
