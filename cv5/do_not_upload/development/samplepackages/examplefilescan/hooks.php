<?php
class examplefilescan_Hooks
{
	public static function hookAdminSettingsSelectOptions($params)
	{
		if($params['settingid'] == 'enabled_scanner')
		{
			$phrasesToFetch = array(
				'examplefilescan_blockall_label',
				'examplefilescan_blockwords_label',
			);
			$vbphrase = vB_Api::instanceInternal('phrase')->fetch($phrasesToFetch);
			$params['options']['examplefilescan:blockall'] = $vbphrase['examplefilescan_blockall_label'];
			$params['options']['examplefilescan:blockword'] = $vbphrase['examplefilescan_blockwords_label'];
		}
	}
}
