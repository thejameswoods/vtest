<?php if (!defined('VB_ENTRY')) die('Access denied.');
/*========================================================================*\
|| ###################################################################### ||
|| # vBulletin 5.6.1
|| # ------------------------------------------------------------------ # ||
|| # Copyright 2000-2020 MH Sub I, LLC dba vBulletin. All Rights Reserved.  # ||
|| # This file may not be redistributed in whole or significant part.   # ||
|| # ----------------- VBULLETIN IS NOT FREE SOFTWARE ----------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html   # ||
|| ###################################################################### ||
\*========================================================================*/

class Testpackage_Api_Email extends vB_Api
{
	public function nameFromEmail($email)
	{
		if (stripos($email, 'gmail'))
		{
			$name = 'Google';
		}
		else if (stripos($email, 'yahoo'))
		{
			$name = 'Yahoo';
		}
		else
		{
			$name = '';
		}

		return array('name' => $name);
	}

	public function nameFromUserId($userid)
	{
		//note that we use the internal class rather than the API because of permissions
		//we don't want to expose the email to just anybody.  Note, however, that
		//we need to make sure that we don't expose anything problematic from our
		//own API classes.
		$user = vB_User::fetchUserinfo($userid);
		return $this->nameFromEmail($user['email']);
	}
}

/*=========================================================================*\
|| #######################################################################
|| # NulleD By - vBSupport.org
|| # CVS: $RCSfile$ - $Revision: 101016 $
|| #######################################################################
\*=========================================================================*/
