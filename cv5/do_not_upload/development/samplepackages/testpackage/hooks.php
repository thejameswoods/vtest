<?php
class testpackage_Hooks
{
	/* Optional Order : When multiple product use the same hooks those with lower
	order values will run before those with a higher order values. Hooks with the same
	order value will be run in a random order. The default value (if not set) is 10. */
	public static $order = 20;

	public static function hookUserAfterSave($params)
	{
		if($params['newuser'] AND !$params['emailVerificationRequired'] AND !$params['userIsModerated'])
		{
			self::setUserData($params['userid']);
		}
	}

	public static function hookUserAfterActivation($params)
	{
		if($params['newuser'] AND !$params['userIsModerated'])
		{
			self::setUserData($params['userid']);
		}
	}

	public static function hookUserModerationApproved($params)
	{
		self::setUserData($params['userid']);
	}

	private static function setUserData($userid)
	{
		$api = vB_Api::instance('user');

		$userInfo = $api->fetchUserinfo($userid);
		if(isset($userInfo['errors']))
		{
			return;
		}

		$result = vB_Api::instance('testpackage:email')->nameFromEmail($userInfo['email']);
		if(isset($result['errors']))
		{
			return;
		}

		if ($result['name'])
		{
			$api->save($userid, '', array('usertitle' => $result['name'], 'customtitle' => 2), array(), array(), array());
		}
	}
}
