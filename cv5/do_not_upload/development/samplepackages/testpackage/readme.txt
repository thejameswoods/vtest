This is a sample package implementing a vbulletin add on product.  The product
will do two things.

1) If a user registers with an gmail or yahoo email address, the user's title
will automatically be set to "Google" or "Yahoo".  But only after activation
or moderation is completed.

2) Regardless of title, a message for those users will display in the user
stat blocks on posts.

This isn't really intended to be useful, but it demonstrates several add on
concepts:

1) Structure of a package directory
2) Code hooks
3) Template hooks
4) Custom API classes

To see the product in action, copy this entire directory to the core/packages
directory.  The product is set to auto install on the next pageload (this is
not recommended for active sites).
